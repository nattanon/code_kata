const balanceStatements = (str) => {
  const opjValue = str.split(', ').reduce((opjResult, current) => {
    currentArray = current.split(' ')
    if (validation(currentArray))
      opjResult[currentArray[3]] += parseInt(parseFloat(currentArray[1]) * parseFloat(currentArray[2]));
    else
      opjResult['Badly'].push(current)
    return opjResult
  }, {
    'B': 0,
    'S': 0,
    'Badly': []
  });
  return transformToStr(opjValue)
}

const transformToStr = (opjValue) => {
  let strReturn = `Buy: ${opjValue['B']} Sell: ${opjValue['S']}`
  if (opjValue['Badly'].length > 0) {
    strReturn += `; Badly formed ${opjValue['Badly'].length}: ${opjValue['Badly'].join(' ; ')} ;`
  }
  return strReturn
}

const validation = (valueArray) => {
  const regInt = new RegExp('^[0-9]+$');
  const regFloat = new RegExp('^[0-9]+\.[0-9]+$');
  if (valueArray.length != 4)
    return false
  else if (!regInt.test(valueArray[1]))
    return false
  else if (!regFloat.test(valueArray[2]))
    return false
  else if (!['B', 'S'].includes(valueArray[3]))
    return false
  else
    return true
}

module.exports = balanceStatements