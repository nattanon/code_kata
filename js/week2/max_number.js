const maxNumber = (number) => {
  const strNumber = number.toString()
  let counting = Array.from({
      length: 9
    }, () => ''),
    i = strNumber.length
  while (i--) {
    counting[Math.abs(parseInt(strNumber.charAt(i)) - 9)] += strNumber.charAt(i)
  }
  return parseInt(counting.join(''))
}

// const maxNumber = (number) => {
//   return parseInt(number.toString().split('').map(Number).sort((a, b)=> b - a).join(''));
// }

// const maxNumber = (number) => {
//   return parseInt(countingSort(number.toString().split('').map(Number)).join(''));
// }

// function countingSort(arr) {
//   let i, z = 0,
//     count = []
//   for (i = 1; i <= 9; i++) {
//     count[i] = 0;
//   }
//   for (i = 0; i < arr.length; i++) {
//     count[arr[i]]++;
//   }
//   for (i = 9; i >= 1; i--) {
//     while (count[i]-- > 0) {
//       arr[z++] = i;
//     }
//   }
//   return arr;
// }

// function quickSort(origArray) {
// 	if (origArray.length <= 1) { 
// 		return origArray;
// 	} else {
// 		let left = [];
// 		let right = [];
// 		let newArray = [];
// 		let pivot = origArray.pop();
// 		let length = origArray.length;
// 		for (let i = 0; i < length; i++) {
// 			if (origArray[i] >= pivot) {
// 				left.push(origArray[i]);
// 			} else {
// 				right.push(origArray[i]);
// 			}
// 		}
// 		return newArray.concat(quickSort(left), pivot, quickSort(right));
// 	}
// }

module.exports = maxNumber