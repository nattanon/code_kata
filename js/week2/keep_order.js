const keepOrder = (ary, val) => {
  let low = 0
  let radius = ary.length - 1
  let min = 0
  while(low <= radius){
    min = Math.floor((low + radius) / 2)
    if(ary[min] < val)
      low = min + 1
    else if (ary[min] > val)
      radius = min - 1
    else
      break
  }
  return min
}

module.exports = keepOrder
