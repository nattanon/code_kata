const reverseOrder = (list) => {
  return list.reverse()
}

module.exports = reverseOrder
