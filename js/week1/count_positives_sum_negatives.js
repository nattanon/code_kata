const countPositivesSumNegatives = (list) => {
  var data_return = [0,0]
  if (list && list.length)
    list.forEach(function(value) {
      if(value >= 0){
        data_return[0] ++
      }else{
        data_return[1] += value
      }
    })
  else{
    data_return = []
  }
  return data_return;
}

module.exports = countPositivesSumNegatives
