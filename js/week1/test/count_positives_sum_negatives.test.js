const countPositivesSumNegatives = require('../count_positives_sum_negatives')

test('Should return [10, -65] when list is [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15]', () => {
  expect(countPositivesSumNegatives([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15])).toEqual([10,-65])
})

test('Should return [10, -50] when list is [0, 2, 3, 0, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14]', () => {
  expect(countPositivesSumNegatives([0, 2, 3, 0, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14])).toEqual([10,-50])
})

test('Should return [1, 0] when list is[1]', () => {
  expect(countPositivesSumNegatives([1])).toEqual([1,0])
})

test('Should return [0, -1] when list is [-1]', () => {
  expect(countPositivesSumNegatives([-1])).toEqual([0,-1])
})

test('Should return [8, 0] when list is [0, 0, 0, 0, 0, 0, 0, 0, 0]', () => {
  expect(countPositivesSumNegatives([0,0,0,0,0,0,0,0])).toEqual([8,0])
})
