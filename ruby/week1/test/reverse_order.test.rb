require_relative '../reverse_order'

describe 'Reverse Order' do
  it 'Should return [5, 1, 2, 7, 9] when list is [9, 7, 2, 1, 5]' do
    assert_equal reverse_order([9, 7, 2, 1, 5]), [5, 1, 2, 7, 9]
  end

  it 'Should return [3, 1, 4, 5, 6, 8] when list is [8, 6, 5, 4, 1, 3]' do
    assert_equal reverse_order([8, 6, 5, 4, 1, 3]), [3, 1, 4, 5, 6, 8]
  end
end
