require_relative '../count_positives_sum_negatives'

describe 'Count positives/Sum negatives' do
  it 'Should return [10, -65] when list is [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15]' do
    assert_equal count_positives_sum_negatives([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15]), [10,-65]
  end

  it 'Should return [8, -50] when list is [0, 2, 3, 0, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14]' do
    assert_equal count_positives_sum_negatives([0, 2, 3, 0, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14]), [8, -50]
  end

  it 'Should return [1, 0] when list is [1]' do
    assert_equal count_positives_sum_negatives([1]), [1, 0]
  end

  it 'Should return [0, -1] when list is [-1]' do
    assert_equal count_positives_sum_negatives([-1]), [0, -1]
  end

  it 'Should return [0, 0] when list is [0, 0, 0, 0, 0, 0, 0, 0, 0]' do
    assert_equal count_positives_sum_negatives([0, 0, 0, 0, 0, 0, 0, 0, 0]), [0, 0]
  end
end
