require 'minitest/autorun'

Dir.glob('week1/test/*.test.rb') do |f|
  require(File.join('.', f))
end
