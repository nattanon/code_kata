def count_positives_sum_negatives(list)
  data_return = [0, 0]
  if !list.nil? && list.length >= 1
    list.each do |v|
      if v > 0
        data_return[0] += 1
      else
        data_return[1] += v
      end
    end
  else
    data_return = []
  end
  data_return
end
