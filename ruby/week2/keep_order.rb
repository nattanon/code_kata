def keep_order(ary, val)
  low = 0
  radius = ary.length - 1
  min = 0
  while low <= radius
    min = ((low + radius) / 2).floor
    if ary[min] < val
      low = min + 1
    elsif ary[min] > val
      radius = min - 1
    else
      break
    end
  end
  min
end
