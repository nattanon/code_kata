# Code Kata
---
A code kata is an exercise in programming which helps programmers hone their skills through practice and repetition. ~ Wiki

# How to run test
## JS
- Go to js directory
- Install libraries 
```
  $ npm install
```
- Testing with Jest
```
  npm test -- week2
```

## Ruby
- Go to ruby directory
- Install libraries
```
  $ bundle install
```

- Testing with minitest
```
  $ rake test[2]
```

# Week 2
## Keep the Order
สร้างฟังก์ชัน `keepOrder (JS)` หรือ `keep_order (Ruby)` ที่รับพารามิเตอร์ 2 ตัวคือ
- ary ซึ่งเป็น array ที่เรียงลำดับแล้วจากน้อยไปมาก และสามารถมีเลขซ้ำกันได้
- val ซึ่งเป็นค่าที่จะนำไปใส่ใน ary

### Note
ห้ามทำการแก้ไข ary

### Example
```
keep_order([1, 2, 3, 4, 7], 5) -> 4
                        ^(index 4)
keep_order([1, 2, 3, 4, 7], 0) -> 0
            ^(index 0)
keep_order([1, 1, 2, 2, 2], 2) -> 2
                  ^(index 2)
```

### Assertion
```
  assert_equal keep_order([1, 2, 3, 4, 7], 5), 4
  assert_equal keep_order([1, 2, 3, 4, 7], 0), 0
  assert_equal keep_order([1, 1, 2, 2, 2], 2), 2
  assert_equal keep_order([1, 2, 3, 4], -1), 0
  assert_equal keep_order([1, 2, 3, 4], 2), 1
  assert_equal keep_order([], 2), 0
```
---

# Form The Largest
สร้างฟังก์ชัน `max_number (JS)` หรือ `maxNumber (Ruby)` โดยรับพารามิเตอร์เป็นตัวเลข และทำการคืนค่าเป็นตัวเลข
ที่มีค่าสูงสุดซึ่งเกิดจากตัวเลขที่ได้มาสร้างใหม่

## Note
- ตัวเลขที่เป็นพารามิเตอร์จะเป็นค่าบวกเท่านั้น
- ตัวเลขจะมีค่า 1-9 ไม่มีการใช้เลข 0

## Example
```
  maxNumber(213) -> 321
  maxNumber(63729) -> 97632
  maxNumber(566797) -> 977665
```
---

# Stockbroker
กำหนดให้ผู้ซื้อหุ้นทำการสั่งซ์้อหุ้นผ่านนายหน้า โดยส่งคำสั่งมาเป็นข้อความเดี่ยวหรือเป็นชุดต่อกัน

ซึ่งมีรูปแบบดังนี้
```
  Quote/white-space/Quantity/white-space/Price/white-space/Status
```

- Quote -> ชื่อหุ้นเป็นตัวอักษรที่ห้ามมีช่องว่าง (string)
- Quantity -> จำนวนหุ้น (int)
- Price -> ราคาหุ้น ซึ่งมีจุดทศนิยม (float, double)
- Status -> สถานะการซื้อ (B) หรือขาย (S)

เพื่อความสะดวกของนายหน้าจะมีการตรวจสอบคำสั่ง และสรุปราคาการสั่งซื้อและขายผ่านฟังก์ชัน balance_statements ที่รับพารามิเตอร์เป็นคำสั่ง และทำการคืนค่าเป็นผลสรุปการสั่งซื้อและขายที่มีรูปแบบดังนี้
```
  Buy: B Sell: S
```

- B -> ราคารวมการซื้อ เป็นตัวเลข (int)
- S -> ราคารวมการขาย เป็นตัวเลข (int)

ในกรณีที่ผิดพลาดลูกค้าอาจจะส่งคำสั่งมาผิด ฟังก์ชันจะทำการแสดงผลจำนวนคำสั่งที่ผิดพลาด และคำสั่งที่ผิดพลาดออกมาให้เห็น โดยมีรูปแบบดังนี้
```
  Buy: b Sell: s; Badly formed nb: badly-formed 1st simple order ; badly-formed nth simple order ;
```

## Example
```
balance_statements("GOOG 300 542.0 B") -> "Buy: 162600.0 Sell: 0"
balance_statements("ZNGA 1300 2.66 B, CLH15.NYM 50 56.32 B, OWW 1000 11.623 B, OGG 20 580.1 B") -> "Buy: 29499 Sell: 0"
balance_statements("GOOG 300 542.0 B, AAPL 50 145.0 B, CSCO 250.0 29 B, GOOG 200 580.0 S") -> "Buy: 169850 Sell: 116000; Badly formed 1: CSCO 250.0 29 B ;"
```
---

# Week 1
## Set Alarm (set_alarm)
สร้างฟังก์ชัน `set_alarm` ซึ่งรับพารามิเตอร์ 2 ตัว คือ employed และ vacation โดยจะมีการคืนค่าผลลัพธ์ตามเงื่อนไขต่อไปนี้ 
- ฟังก์ชันจะคืนค่าผลลัพธ์เป็นจริง เมื่อพารามิเตอร์ employed เป็นจริง และ vacation เป็นเท็จ
- นอกเหนือจากเงื่อนไขข้างต้น จะให้ผลลัพท์เป็นเท็จ

### Example
```
  set_alarm(true, true) -> false
  set_alarm(false, true) -> false
  set_alarm(true, false) -> true
```

### Assertion
```
  assert_equal set_alarm(true, true), false
  assert_equal set_alarm(false, true), false
  assert_equal set_alarm(false, false), false
  assert_equal set_alarm(true, false), true
```
---

## Reverse Order (reverse_order)
สร้างฟังก์ชัน `reverse_order` ซึ่งรับพารามิเตอร์ 1 ตัวเป็น array ของตัวเลข และให้ทําการคืนค่าผลลัพธ์เป็น array ชุดใหม่ที่ได้เรียงลำดับย้อนหลัง

*** ห้ามใช้ฟังก์ชัน built-in ของ Array

### Example
```
  reverse_order([1,2,3,4]) == [4,3,2,1]
  reverse_order([3,1,5,4]) == [4,5,1,3]
```

### Assertion
```
  assert_equal(reverse_order([9,7,2,1,5]), [5,1,2,7,9])
  assert_equal(reverse_order([8,6,5,4,1,3]), [3,1,4,5,6,8])
```
---

## Count positives/Sum negatives(count_positives_sum_negatives)
สร้างฟังก์ชัน `count_positives_sum_negatives` ซึ่งรับพารามิเตอร์ 1 ตัวเป็น array ของตัวเลข
และทําการคืนค่าผลลัพท์เป็น array ที่ประกอบไปด้วยจํานวนนับของตัวเลขที่เป็นค่าบวกในตําแหน่งที่ 1 และผลรวมของตัวเลขที่เป็นค่าลบในตําแหน่งที่ 2
ถ้าพารามิเตอร์ที่รับเป็นค่าว่างหรือ null ให้ทําการคืนค่าเป็น array ว่าง

### Example
```
  count_positives_sum_negatives([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15] → [10, -65]
```

### Assertion
```
  assert_equal(count_positives_sum_negatives([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15]), [10, -65])
  assert_equal(count_positives_sum_negatives([0, 2, 3, 0, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14]), [8, -50])
  assert_equal(count_positives_sum_negatives([1]), [1, 0])
  assert_equal(count_positives_sum_negatives([-1]), [0, -1])
  assert_equal(count_positives_sum_negatives([0,0,0,0,0,0,0,0,0]), [0, 0])
```
